﻿<?php
// アイキャッチ画像を有効にする。
add_theme_support('post-thumbnails');

// 固定ページ本文の自動変換を無効
remove_filter('the_content', 'wpautop');
remove_filter('the_content', 'wptexturize');

function post_has_archive( $args, $post_type ) {

	if ( 'post' == $post_type ) {
		$args['rewrite'] = true;
		$args['has_archive'] = 'blogs';
	}
	return $args;

}
add_filter( 'register_post_type_args', 'post_has_archive', 10, 2 );


// 前・次の記事へ　クラス名付与
function add_prev_post_link_class($output) {
  return str_replace('<a href=', '<a class="prev" href=', $output); //前の記事リンク
}
add_filter( 'previous_post_link', 'add_prev_post_link_class' );
function add_next_post_link_class($output) {
  return str_replace('<a href=', '<a class="next" href=', $output); //次の記事リンク
}
add_filter( 'next_post_link', 'add_next_post_link_class' );


/* ContactForm7 のカスタムバリデーション */
add_filter('wpcf7_validate', 'wpcf7_validate_customize', 11, 2);

function wpcf7_validate_customize($result,$tags){

  foreach( $tags as $tag ){
    $type = $tag['type'];
    $name = $tag['name'];
    $post = trim(strtr((string) $_POST[$name], "\n", ""));

    switch($type){
      case 'tel':
      case 'tel*':
        if( preg_match( '/^0[0-9]{2,5}-[0-9]{1,4}-[0-9]{4,5}$/',$_POST[$name] ) == false){
          $result->invalidate( $name,'ハイフンは省略せず市外局番から入力してください。' );
        }
        break;
    }

  }
  return $result;
}

// 商品一覧　表示順反映
function sortByKey($key_name, $sort_order, $array) {
  foreach ($array as $key => $value) {
      $standard_key_array[$key] = $value[$key_name];
  }

  array_multisort($standard_key_array, $sort_order, $array);

  return $array;
}