// ふわっ
function fadeAnime(){
	$('.fadeUpTrigger').each(function(){
		var elemPos = $(this).offset().top+50;
		var scroll = $(window).scrollTop();
		var windowHeight = $(window).height();
		if (scroll >= elemPos - windowHeight){
			$(this).addClass('fadeUp');
		}else{
			$(this).removeClass('fadeUp');
		}
	});
}
	
$(window).scroll(function (){
	fadeAnime();
});


// 背景画像スイッチャー
$(function(){
	var x = $(window).width();
	var y = 767;
	if (x <= y) {
		$('.slick').removeClass('slick_pc');
	} else {
		$('.slick').addClass('slick_pc');
		$('.slick_pc').slick({
			prevArrow:'<span class="arrow icon_p"></span>',
			nextArrow:'<span class="arrow icon_n"></span>',
			centerPadding: '0px',
			speed: 1000,
			autoplaySpeed: 5000,
			autoplay: true,
			slidesToShow: 3,
			slidesToScroll: 3,
			focusOnSelect: true,
			dots: true,
		});
	}

	$('.slick_store').slick({
		autoplay: true,
		autoplaySpeed: 0,
		speed: 6000,
		cssEase: "linear",
		slidesToShow: 3,
		swipe: false,
		arrows: false,
		pauseOnFocus: false,
		pauseOnHover: false,
		responsive: [
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: 1.3,
				speed: 5000,
			  }
			}
		]
	});

	const swiper = new Swiper('.swiper', {
		loop: true,
	  
		pagination: {
		  el: '.swiper-pagination',
		},
	  
		navigation: {
		  nextEl: '.swiper-button-next',
		  prevEl: '.swiper-button-prev',
		},
	  
	});
});


$(window).resize(function () {
    var x = $(window).width();
	var y = 767;
	if (x <= y) {
		$('.slick').removeClass('slick_pc');
	} else {
		$('.slick').addClass('slick_pc');
		$('.slick_pc').slick({
			prevArrow:'<span class="arrow icon_p"></span>',
			nextArrow:'<span class="arrow icon_n"></span>',
			centerPadding: '0px',
			speed: 1000,
			autoplaySpeed: 5000,
			autoplay: true,
			slidesToShow: 3,
			slidesToScroll: 3,
			focusOnSelect: true,
			dots: true,
		});
	}

	$('.slick_store').slick({
		autoplay: true,
		autoplaySpeed: 0,
		speed: 6000,
		cssEase: "linear",
		slidesToShow: 3,
		swipe: false,
		arrows: false,
		pauseOnFocus: false,
		pauseOnHover: false,
		responsive: [
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: 2,
				speed: 5000,
			  }
			}
		]
	});
});


// トップページ 背景画像切り替え
jQuery(function($) {
    var x = $(window).width();
	var y = 767;
	if (x <= y) { //スマホ
		$('.eyecatch_content').bgSwitcher({
			images: [path+'/img/top-1.jpg',path+'/img/top-5_sp.jpg',path+'/img/top-2_sp.jpg',path+'/img/top-7.jpg'],
			interval: 5000,
			loop: true,
			duration: 2000,
		});
	} else { //PC
		$('.eyecatch_content').bgSwitcher({
			images: [path+'/img/top-1.jpg',path+'/img/top-5_pc.jpg',path+'/img/top-2.jpg',path+'/img/top-7.jpg'],
			interval: 5000,
			loop: true,
			duration: 2000,
		});
	}
});
$(window).resize(function () {
    var x = $(window).width();
	var y = 767;
	if (x <= y) { //スマホ
		$('.eyecatch_content').bgSwitcher({
			images: [path+'/img/top-1.jpg',path+'/img/top-5_sp.jpg',path+'/img/top-2_sp.jpg',path+'/img/top-7.jpg'],
			interval: 5000,
			loop: true,
			duration: 2000,
		});
	} else { //PC
		$('.eyecatch_content').bgSwitcher({
			images: [path+'/img/top-1.jpg',path+'/img/top-5_pc.jpg',path+'/img/top-2.jpg',path+'/img/top-7.jpg'],
			interval: 5000,
			loop: true,
			duration: 2000,
		});
	}
});


// メニューボタン
$(function(){
	$(".openbtn1").click(function () {
		$(this).toggleClass('active');
		$("#g-nav").toggleClass('panelactive');
	});

	$("#g-nav a").click(function () {
		$(".openbtn1").removeClass('active');
		$("#g-nav").removeClass('panelactive');
	});
});



// モーダルウィンドウ
$(function(){

	// モーダルウィンドウを開く
	$('.modal-open').on('click', function(){
		var target = $(this).data('target');
		var modal = document.getElementById(target);
		scrollPosition = $(window).scrollTop();
	
		$('body').addClass('fixed').css({'top': -scrollPosition});
		$(modal).fadeIn();
		return false;
	  });
	
	// モーダルウィンドウを閉じる
	$('.modal-close').on('click', function(){
		$('body').removeClass('fixed');
		window.scrollTo( 0 , scrollPosition );
		$('.modal-wrapper').fadeOut();
		return false;
	});
	$('.modal-overlay').on('click', function(){
		$('body').removeClass('fixed');
		window.scrollTo( 0 , scrollPosition );
		$('.modal-wrapper').fadeOut();
		return false;
	});
});


// トップ ロード画面
(window.onload = function() {
	$('#loading').addClass('loaded');
});


// トップに戻るボタン
$(function(){
	var pagetop = $('#page-top');
	pagetop.hide();
  
	$(window).scroll(function () {
	   if ($(this).scrollTop() > 100) {
			pagetop.fadeIn();
	   } else {
			pagetop.fadeOut();
	   }
	});
	pagetop.click(function () {
	   $('body, html').animate({ scrollTop: 0 }, 500);
	   return false;
	});
});


// スムーズスクロール
var Ease = {
  easeInOut: function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1; }
}
var duration = 1500;
window.addEventListener('DOMContentLoaded', function () {
  var smoothScrollTriggers = [].slice.call(document.querySelectorAll('a[href^="#"]'));
  smoothScrollTriggers.forEach(function (smoothScrollTrigger) {
    smoothScrollTrigger.addEventListener('click', function (e) {
      var href = smoothScrollTrigger.getAttribute('href');
      var currentPostion = document.documentElement.scrollTop || document.body.scrollTop;
      var targetElement = document.getElementById(href.replace('#', ''));
      if (targetElement) {
        e.preventDefault();
        e.stopPropagation();
        var targetPosition = window.pageYOffset + targetElement.getBoundingClientRect().top;
        var startTime = performance.now();
        var loop = function (nowTime) {
          var time = nowTime - startTime;
          var normalizedTime = time / duration;
          if (normalizedTime < 1) {
            window.scrollTo(0, currentPostion + ((targetPosition - currentPostion) * Ease.easeInOut(normalizedTime)));
            requestAnimationFrame(loop);
          } else {
            window.scrollTo(0, targetPosition);
          }
        }
        requestAnimationFrame(loop);
      }
    });
  });
});


// story parallax
$(function(){
	var x = $(window).width();
	var y = 767;

	if (x <= y) { //スマホ
		
	} else { //PC
		var target1 = $(".story_img1");
		var targetPosOT1 = target1.offset().top;
		var target2 = $(".story_img2");
		var targetPosOT2 = target2.offset().top;
		var target3 = $(".story_img3");
		var targetPosOT3 = target3.offset().top;
		var target4 = $(".story_img4");
		var targetPosOT4 = target4.offset().top;
		var windowH = $(window).height();
		var scrollYStart1 = targetPosOT1 - windowH;
		var scrollYStart2 = targetPosOT2 - windowH;
		var scrollYStart3 = targetPosOT3 - windowH;
		var scrollYStart4 = targetPosOT4 - windowH;
	
		var targetFactor = 0.5;

		target1.css('background-position-y', (targetPosOT1 - scrollY) * targetFactor + 'px');

		$(window).on('scroll', function () {
			var scrollY = $(this).scrollTop();
			if(scrollY >= scrollYStart1){
				target1.css('background-position-y', (targetPosOT1 - scrollY) * targetFactor + 'px');
				target1.css('background-position', 'fixed');
			}
			if(scrollY > scrollYStart2){
				target2.css('background-position-y', (targetPosOT2 - scrollY) * targetFactor + 'px');
				target2.css('background-position', 'fixed');
			}
			if(scrollY > scrollYStart3){
				target3.css('background-position-y', (targetPosOT3 - scrollY) * targetFactor + 'px');
				target3.css('background-position', 'fixed');
			}
			if(scrollY > scrollYStart4){
				target4.css('background-position-y', (targetPosOT4 - scrollY) * targetFactor + 'px');
				target4.css('background-position', 'fixed');
			}
		});
	}
});