<?php header("Access-Control-Allow-Origin: *");?>
<!doctype html>
<html lang="ja">
	<head>
		<title><?php bloginfo('name'); ?></title>

		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>;charset=<?php bloginfo('charset'); ?>">
		<meta name="format-detection" content="telephone=no,address=no,email=no">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="Confitta Colorer,コンフィッタ・クロレ,コンフィッタクロレ,クロレ,彩フォンダンタルト,クロレフォンダンタルト,彩 Fondan Tart,彩レアフォンダンタルト,クロレレアフォンダンタルト,彩 Rare Fondan Tart,コンフィッタルトシュー,Confittarte Chou,ドレスフィナンシェ,Dress Financier,トゥールビヨンバウムショコラ,Tourbillon Baum Chocolat,彩コンフィッタワーズ,クロレコンフィッタワーズ,彩 Confittaoise,リースタルト,Wreath tarte,コンフィチュール,京都,京都府,上京区,北野天満宮,タルト,ホールタルト,ダックワーズ,バウムクーヘン,シュークリーム,お菓子,洋菓子,スイーツ,株式会社伍彩菓,伍彩菓">
		<link rel="SHORTCUT ICON" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico">
		<link href="<?php echo get_template_directory_uri(); ?>/css/destyle.css" rel="stylesheet">
		<link href="<?php echo get_template_directory_uri(); ?>/css/swiper-bundle.min.css" rel="stylesheet">
		<link href="<?php echo get_template_directory_uri(); ?>/css/slick.css" rel="stylesheet">
		<link href="<?php echo get_template_directory_uri(); ?>/css/slick-theme.css" rel="stylesheet">
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://use.typekit.net/dqj5hmu.css">

		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Shippori+Mincho+B1:wght@400;700&display=swap" rel="stylesheet">

		<script>
			var path = "<?php echo get_template_directory_uri();?>";
		</script>
		
		<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.1.1.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.4.1.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bgswitcher.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/slick.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/swiper-bundle.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
		<?php wp_head(); ?>

		<meta name="google-site-verification" content="64qsjzz-cgjVgegnkfEOjaQc3yC12yLakSp3uN5viaI" />
	</head>

	<body <?php body_class(); ?>>
	
	<header>
		<?php
		
		if (substr($_SERVER['REQUEST_URI'], -9) == '/colorer/') {
			$css = 'top_btn';
			$sp = 'sp';
		} elseif(substr($_SERVER['REQUEST_URI'], -11) == '/apps/note/') {
			$css = 'top_btn';
			$sp = 'sp';
		} elseif(substr($_SERVER['REQUEST_URI'], 0) == '/') {
			$css = 'top_btn';
			$sp = 'sp';
		} else {
			$sp = 'pages';
		}

            if (substr(home_url(), 0, 16) == 'http://localhost') {
                $code = 'ec72d09911962cc71aef2e9e8f4c646a529d93748583706f5961fcca1e239333';
            } elseif (substr(home_url(), 0, 15) == 'http://bizaz.jp') {
                $code = '6be3f89685dba46977f474d4edca6db4ac1573e6ad9e53595168eb70c4f5b463';
            } elseif (substr(home_url(), 0, 15) == 'https://gosaika') {
				$code = 'bebae537ffa558f7b7b869897f42a73b3d8584d4f6bef37b48dac45f92cbf227';
			} elseif (substr(home_url(), 0, 16) == 'https://confitta') {
				$code = '4906df20217b44ab7091d5ed2155b6bd13d5e03e35c40a845bce9ed29ad40598';
			}
			
            // 接続
            $request_options = array(
                'http' => array(
                    'method' => 'GET',
                    'header'=> "Authorization: Bearer " . $code
                ),
                'ssl'=> array(
                    'verify_peer'=>false,
                    'verify_peer_name'=>false,
                ),
            );
            $context = stream_context_create($request_options);

			// ショップ情報取得
            $url_shop = 'https://api.shop-pro.jp/v1/shop.json';
            $response_shop = file_get_contents($url_shop, false, $context);
            $shop = json_decode($response_shop, true);

		?>

		<!-- sp -->
		<div class="openbtn1 <?php echo $css; ?>">
			<span></span><span></span><span></span>
		</div>
		<nav id="g-nav" class="sp">
			<a href="#" class="menu-close sp"></a>
			<div id="g-nav-list">
				<img class="logo sp" src="<?php echo get_template_directory_uri(); ?>/img/colorer_logo_bl.png" alt="colorer">
				<ul>
					<li class="pc h1_logo">
						<h1 class="top-h1">
							<a href="<?php echo home_url(); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/img/colorer_logo_bl.png" alt="colorer">
							</a>
						</h1>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>">
							top
						</a>
					</li>
					<li>
						<a href="<?php echo home_url('/story/'); ?>">
							story
						</a>
					</li>
					<li>
						<a href="<?php echo home_url('/menu/'); ?>">
							menu
						</a>
					</li>
					<li>
						<a href="<?php echo home_url('/store/'); ?>">
							store
						</a>
					</li>
					<li>
						<a href="<?php echo home_url('/category/news/'); ?>">
							news
						</a>
					</li>
					<li>
						<a href="<?php echo home_url('/category/blog/'); ?>">
							blog
						</a>
					</li>
					<li>
						<a href="<?php echo home_url('/contact/'); ?>">
							contact
						</a>
					</li>
				</ul>
				<a href="<?php echo $shop['shop']['url']; ?>" class="onlineshop_bunner sp" target="_blank">
					<div><img src="<?php echo get_template_directory_uri(); ?>/img/dummy-2.jpg" alt="オンラインショップ"></div>
					<p>ONLINE SHOP</p>
				</a>
			</div>
		</nav>

		<!-- pc -->
		<nav class="nav-pc pc <?php echo $sp; ?>">
			<ul>
				<li class="pc h1_logo">
					<h1 class="top-h1">
						<a href="<?php echo home_url(); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/colorer_logo_bl.png" alt="colorer">
						</a>
					</h1>
				</li>
				<li>
					<a href="<?php echo home_url(); ?>">
						top
					</a>
				</li>
				<li>
					<a href="<?php echo home_url('/story/'); ?>">
						story
					</a>
				</li>
				<li>
					<a href="<?php echo home_url('/menu/'); ?>">
						menu
					</a>
				</li>
				<li>
					<a href="<?php echo home_url('/store/'); ?>">
						store
					</a>
				</li>
				<li>
					<a href="<?php echo home_url('/category/news/'); ?>">
						news
					</a>
				</li>
				<li>
					<a href="<?php echo home_url('/category/blog/'); ?>">
						blog
					</a>
				</li>
				<li>
					<a href="<?php echo home_url('/contact/'); ?>">
						contact
					</a>
				</li>
			</ul>
			<a href="<?php echo $shop['shop']['url']; ?>" class="shop_btn pc" target="_blank">ONLINE SHOP</a>
		</nav>
	</header>