<?php get_header(); ?>

<?php
	if (substr(home_url(), 0, 16) == 'http://localhost') {
		$code = 'ec72d09911962cc71aef2e9e8f4c646a529d93748583706f5961fcca1e239333';
	} elseif (substr(home_url(), 0, 15) == 'http://bizaz.jp') {
		$code = '6be3f89685dba46977f474d4edca6db4ac1573e6ad9e53595168eb70c4f5b463';
	} elseif (substr(home_url(), 0, 15) == 'https://gosaika') {
		$code = 'bebae537ffa558f7b7b869897f42a73b3d8584d4f6bef37b48dac45f92cbf227';
	} elseif (substr(home_url(), 0, 16) == 'https://confitta') {
		$code = '4906df20217b44ab7091d5ed2155b6bd13d5e03e35c40a845bce9ed29ad40598';
	}

	// 接続
	$request_options = array(
		'http' => array(
			'method' => 'GET',
			'header'=> "Authorization: Bearer " . $code
		),
		'ssl'=> array(
			'verify_peer'=>false,
			'verify_peer_name'=>false,
		),
	);
	$context = stream_context_create($request_options);

	// ショップ情報取得
	$url_shop = 'https://api.shop-pro.jp/v1/shop.json';
	$response_shop = file_get_contents($url_shop, false, $context);
	$shop = json_decode($response_shop, true);
?>

<!-- ロード画面 -->
<div id="loading">
	<img src="<?php echo get_template_directory_uri(); ?>/img/colorer_logo_bl.png" alt="colorer">
	<small>Confitta Colorer［コンフィッタ・クロレ］</small>
</div>

<main>
	<section class="eyecatch">
		<div class="eyecatch_content">
			<nav class="nav-top">
				<ul>
					<li class="h1_logo">
						<h1 class="top-h1">
							<a href="<?php echo home_url(); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/img/colorer_logo_bl.png" alt="colorer">
							</a>
						</h1>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>">
							top
						</a>
					</li>
					<li>
						<a href="<?php echo home_url('/story/'); ?>">
							story
						</a>
					</li>
					<li>
						<a href="<?php echo home_url('/menu/'); ?>">
							menu
						</a>
					</li>
					<li>
						<a href="<?php echo home_url('/store/'); ?>">
							store
						</a>
					</li>
					<li>
						<a href="<?php echo home_url('/category/news/'); ?>">
							news
						</a>
					</li>
					<li>
						<a href="<?php echo home_url('/category/blog/'); ?>">
							blog
						</a>
					</li>
					<li>
						<a href="<?php echo home_url('/contact/'); ?>">
							contact
						</a>
					</li>
				</ul>
				<a href="<?php echo $shop['shop']['url']; ?>" class="shop_btn top_shop_btn" target="_blank">ONLINE SHOP</a>
			</nav>
			<div class="copy">
				<img class="sp" src="<?php echo get_template_directory_uri(); ?>/img/colorer_logo_wh.svg" alt="Confitta Colorer ロゴ">
				<p class="main_copy">
					<img src="<?php echo get_template_directory_uri(); ?>/img/top_copy.png" alt="北海道から宮古島まで全国の農家を訪ねて生まれるコンフィチュールやスイーツ">
				</p>
				<p class="sub_copy pc">
					Confitta Colorer［コンフィッタ・クロレ］の<br>
					名物コンフィチュールは<br>
					農薬をあまり使わない、酸味や苦味のある<br>
					昔ながらのフルーツを全国の農家さんから<br>
					直接仕入れて作っています。<br>
					新たな農家さんとの繋がりが増えるにつれ<br>
					新作も次々に登場しておりますので、<br>
					訪れるたびに新たな商品との“出会い”を<br>
					お楽しみいただけます。<br>
				</p>
			</div>
		</div>
	</section>

	<section>
		<p class="main_copy_sp">
			<img src="<?php echo get_template_directory_uri(); ?>/img/top_copy_black.png" alt="北海道から宮古島まで全国の農家を訪ねて生まれるコンフィチュールやスイーツ">
		</p>
		<p class="sub_copy_sp sp">
			Confitta Colorer［コンフィッタ・クロレ］の<br>
			名物コンフィチュールは<br>
			農薬をあまり使わない、酸味や苦味のある<br>
			昔ながらのフルーツを全国の農家さんから<br>
			直接仕入れて作っています。<br>
			新たな農家さんとの繋がりが増えるにつれ<br>
			新作も次々に登場しておりますので、<br>
			訪れるたびに新たな商品との“出会い”を<br>
			お楽しみいただけます。<br>
		</p>
	</section>

	<div class="bg_container">
		<section class="tarte">
			<h2 class="sp fadeUpTrigger" style="line-height:2em; font-size: 17px;">5人の職人が創る<br>タルトと焼き菓子専門店“<img class="colorer" src="<?php echo get_template_directory_uri(); ?>/img/colorer_name.png" alt="コンフィッタクロレ">”</h2>
			<div class="layout">
				<div class="layout_right_sp sp fadeUpTrigger">
					<img src="<?php echo get_template_directory_uri(); ?>/img/top-2.jpg" alt="5人の職人が創るタルトと焼き菓子専門店 “Confitta(コンフィッタ) Colorer(クロレ)”">
				</div>
				<div class="layout_left">
					<div class="left_content fadeUpTrigger">
						<h2 class="pc" style="line-height:2em; font-size: clamp(16px, 1.5vw, 24px);">5人の職人が創る<br>タルトと焼き菓子専門店“<img class="colorer" src="<?php echo get_template_directory_uri(); ?>/img/colorer_name.png" alt="コンフィッタクロレ">”</h2>
						<p>“Confitta”＝Confiture×tart</p>
						<p>コンフィチュール専門店“北野ラボ”が長年培ってきた、<br>フルーツ農家さんとの繋がりにより手に入る<br><b>「本物の果物」</b>を使い、職人が創り上げる<br><b>“コンフィチュールを楽しむ”新感覚のタルトと焼き菓子。</b></p>
						<p>一口食べると口の中に広がる、素材本来のもつ<br>“香り・風味・食感”は素晴らしく、思わず眼を閉じて<br>堪能したくなるようなお菓子に仕上げております。</p>
					</div>
					<div class="layout_right pc fadeUpTrigger"></div>
				</div>
			</div>
		</section>
		<section class="farmer">
			<h2 class="sp fadeUpTrigger" style="font-size: 17px;">全国の農家さんの研究と<br>努力の結晶を伝える“コンフィチュール”</h2>
			<div class="layout reverse">
				<div class="layout_right_sp sp fadeUpTrigger">
					<img class="farmer_img1" src="<?php echo get_template_directory_uri(); ?>/img/top-4.jpg" alt="全国の農家さんの研究と努力の結晶を伝えるコンフィチュール">
					<img class="farmer_img2" src="<?php echo get_template_directory_uri(); ?>/img/story/story-7.jpg" alt="全国の農家さんの研究と努力の結晶を伝えるコンフィチュール">
					<img class="farmer_img3" src="<?php echo get_template_directory_uri(); ?>/img/top-6.jpg" alt="全国の農家さんの研究と努力の結晶を伝えるコンフィチュール">
				</div>
				<div class="layout_left">
					<div class="layout_right pc fadeUpTrigger">
						<img class="farmer_img1" src="<?php echo get_template_directory_uri(); ?>/img/top-4.jpg" alt="全国の農家さんの研究と努力の結晶を伝えるコンフィチュール">
						<img class="farmer_img2" src="<?php echo get_template_directory_uri(); ?>/img/story/story-7.jpg" alt="全国の農家さんの研究と努力の結晶を伝えるコンフィチュール">
						<img class="farmer_img3" src="<?php echo get_template_directory_uri(); ?>/img/top-6.jpg" alt="全国の農家さんの研究と努力の結晶を伝えるコンフィチュール">
					</div>
					<div class="left_content fadeUpTrigger">
						<h2 class="pc" style="font-size: clamp(16px, 1.5vw, 24px);">全国の農家さんの研究と<br>努力の結晶を伝える“コンフィチュール”</h2>
						<p>コンフィチュールとは、<br>フランス語「confit(砂糖などに漬け込む)」が語源で、<br>煮詰めて作るジャムよりも果実感を楽しむことができ、<br><b>フルーツ本来のフレッシュな味わい</b>を感じられます。</p>
						<p>Confitta Colorer［コンフィッタ・クロレ］では、<br>北海道のハスカップから宮古島のマンゴーまで、<br><b>全国の農家さんがこだわりと情熱を注いで育てられた<br>素材</b>を使って、フルーツ本来の香りや風味を<br>なるべくそのまま感じられるよう、職人が丁寧に<br>作り上げたコンフィチュールを使用しています。</p>
					</div>
				</div>
			</div>
		</section>
		<section class="confiture">
			<h2 class="sp fadeUpTrigger" style="font-size: 17px;"><ruby>コンフィチェリエ<rt>コンフィチュール職人</rt></ruby>とパティシエが創り上げる<br>新感覚のタルトと焼き菓子</h2>
			<div class="layout">
				<div class="layout_right_sp sp fadeUpTrigger">
					<img class="farmer_img1" src="<?php echo get_template_directory_uri(); ?>/img/top-3.jpg" alt="コンフィチェリエとパティシエが創り上げる新感覚のタルトと焼き菓子">
					<img class="farmer_img2" src="<?php echo get_template_directory_uri(); ?>/img/top-8.jpg" alt="コンフィチェリエとパティシエが創り上げる新感覚のタルトと焼き菓子">
					<img class="farmer_img3" src="<?php echo get_template_directory_uri(); ?>/img/top-9.jpg" alt="コンフィチェリエとパティシエが創り上げる新感覚のタルトと焼き菓子">
				</div>
				<div class="layout_left">
					<div class="left_content fadeUpTrigger">
						<h2 class="pc" style="font-size: clamp(16px, 1.5vw, 24px);"><ruby>コンフィチェリエ<rt>コンフィチュール職人</rt></ruby>とパティシエが創り上げる<br>新感覚のタルトと焼き菓子</h2>
						<p>“素材本来の風味が詰まったコンフィチュールの美味しさを<br>新しいカタチで伝えたい“</p>
						<p>そんな職人の想いから、<b>全国の農家さんを訪れ、</b><br>出合った唯一無二の素材を持ち帰り、<br><b>コンフィチェリエが丁寧に仕上げることにより完成する、<br>素材が持つ個性がギュッと詰まった“コンフィチュール”</b>。</p>
						<p>そして、<b>パティシエが試行錯誤しながら<br>何度もの試作を重ねた</b>末に、<br>遂に誕生した、<b>コンフィチュールが主役の<br>“タルトと焼き菓子”</b>の数々…</p>
						<p>一口食べると口の中に広がる香り、風味、食感は<br>きっと今までに体験したことのない<br>新しいものになるはずです。</p>
						<p>5人の職人と農家さんの想いが詰まった<br>新感覚のタルトと焼き菓子たちが、<br>あなたと出合える日を楽しみにしています。</p>
						<a href="<?php echo home_url('/story/'); ?>" class="button fadeUpTrigger">5人の職人達の紹介はこちら</a>
					</div>
					<div class="layout_right pc fadeUpTrigger">
						<img class="farmer_img1" src="<?php echo get_template_directory_uri(); ?>/img/top-3.jpg" alt="コンフィチェリエとパティシエが創り上げる新感覚のタルトと焼き菓子">
						<img class="farmer_img2" src="<?php echo get_template_directory_uri(); ?>/img/top-8.jpg" alt="コンフィチェリエとパティシエが創り上げる新感覚のタルトと焼き菓子">
						<img class="farmer_img3" src="<?php echo get_template_directory_uri(); ?>/img/top-9.jpg" alt="コンフィチェリエとパティシエが創り上げる新感覚のタルトと焼き菓子">
					</div>
				</div>
			</div>
		</section>
	</div>

	<section class="instagram">
		<h2>Instagram Gallery</h2>
		<div class="fadeUpTrigger"><?php echo do_shortcode('[instagram-feed]'); ?></div>
	</section>

	<div class="bg_container_">
		<section class="news">
			<h2>news</h2>
			<ul class="news_ul fadeUpTrigger">
			<?php

				// カテゴリー取得
				$categories = get_categories();

				foreach ($categories as $cat) {
					if ($cat->name == 'news') {
						$cat_news_id = $cat->term_id;
					}
					if ($cat->name == 'blog') {
						$cat_blog_id = $cat->term_id;
					}
				}

				$args = Array(
					'post_type' => 'post',
					'posts_per_page' => 4,
					'order' => 'DESC',
					'cat' => $cat_news_id
				);
				$pickupitem = new WP_Query($args);
				if($pickupitem -> have_posts()) :
					$count = 0;
				while ($pickupitem -> have_posts()) : $pickupitem -> the_post();
			?>
				<li>
					<time><?php the_time('Y.m.d'); ?></time>
					<a href="<?php echo $pickupitem->posts[$count]->guid; ?>" class="contents">
						<div class="news-img">
                        <?php
                        
                        if (the_post_thumbnail('full')) {
                            echo the_post_thumbnail('full'); 
                        } else {
                            echo '<img class="s_logo" src="' . get_template_directory_uri() . '/img/colorer_logo_wh.png" alt="サムネイル">';
                        }
                        
                        ?>
						</div>
						<div class="news-caption">
							<?php
							$posttags = get_the_tags();
							if ( $posttags ) {
								echo '<div class="captions">';
								foreach ( $posttags as $tag ) {
									if ($tag->name == '新商品') {
										$css = 'tag_new';
									} elseif ($tag->name == 'その他') {
										$css = 'tag_other';
									} else {
										$css = '';
									}
									echo '<span class="tag '. $css .'">' . $tag->name .'</span>';
								}
								echo '</div>';
							}
							?>
							<h2><?php echo the_title(); ?></h2>
						</div>
					</a>
				</li>
			<?php
				$count++;
				endwhile;
			?>
			<?php wp_reset_query(); ?>
			</ul>
			<a href="<?php echo home_url('/category/news/'); ?>" class="button fadeUpTrigger">see more</a>
			<?php
				else : echo '<p style="margin-bottom: 40px;">お知らせはありません。</p>';
				endif;
			?>
		</section>
		<section class="blog">
			<h2>blog</h2>
			<ul class="news_ul fadeUpTrigger">
			<?php
				$args = Array(
					'post_type' => 'post',
					'posts_per_page' => 4,
					'order' => 'DESC',
					'cat' => $cat_blog_id
				);
				$pickupitem = new WP_Query($args);
				if($pickupitem -> have_posts()) :
					$count = 0;
				while ($pickupitem -> have_posts()) : $pickupitem -> the_post();
			?>
				<li>
					<time><?php the_time('Y.m.d'); ?></time>
					<a href="<?php echo $pickupitem->posts[$count]->guid; ?>" class="contents">
						<div class="news-img">
                        <?php
                        
                        if (the_post_thumbnail('full')) {
                            echo the_post_thumbnail('full'); 
                        } else {
                            echo '<img class="s_logo" src="' . get_template_directory_uri() . '/img/colorer_logo_wh.png" alt="サムネイル">';
                        }
                        
                        ?>
						</div>
						<div class="news-caption">
							<div class="captions">
							</div>
							<h2><?php echo the_title(); ?></h2>
						</div>
					</a>
				</li>
			<?php
				$count++;
				endwhile;
			?>
			<?php wp_reset_query(); ?>
			</ul>
			<a href="<?php echo home_url('/category/blog/'); ?>" class="button fadeUpTrigger">see more</a>
			<?php
				else : echo '<p>ブログはありません。</p>';
				endif;
			?>
		</section>

		<a href="<?php echo $shop['shop']['url']; ?>" class="onlineshop_bunner index_bunner fadeUpTrigger" target="_blank">
			<div><img src="<?php echo get_template_directory_uri(); ?>/img/dummy-2.jpg" alt="オンラインショップ"></div>
			<p>ONLINE SHOP</p>
		</a>

	</div>

</main>
<?php get_footer(); ?>