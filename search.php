<?php get_header();?>

<div class="detail_big_container">
	<div class="detail_container">
		<?php if (have_posts()): ?>
		<?php
			if (isset($_GET['s']) && empty($_GET['s'])) {
				echo '<p>検索キーワードが未入力です。</p>';
			} else {
				echo '<h3>「'.$_GET['s'] .'」の検索結果：'.$wp_query->found_posts .'件</h3>';
			}
		?>
		<ul class="search_ul">
			<?php while(have_posts()): the_post(); ?>
			<li>
				<a class="arrow" href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a>
				<p><?php the_permalink(); ?></p>
			</li>
			<?php endwhile; ?>
		</ul>
		<?php else: ?>
			検索されたキーワードにマッチする記事はありませんでした。
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>