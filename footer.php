<?php

	if (substr(home_url(), 0, 16) == 'http://localhost') {
		$code = 'ec72d09911962cc71aef2e9e8f4c646a529d93748583706f5961fcca1e239333';
	} elseif (substr(home_url(), 0, 15) == 'http://bizaz.jp') {
		$code = '6be3f89685dba46977f474d4edca6db4ac1573e6ad9e53595168eb70c4f5b463';
	} elseif (substr(home_url(), 0, 15) == 'https://gosaika') {
		$code = 'bebae537ffa558f7b7b869897f42a73b3d8584d4f6bef37b48dac45f92cbf227';
	} elseif (substr(home_url(), 0, 16) == 'https://confitta') {
		$code = '4906df20217b44ab7091d5ed2155b6bd13d5e03e35c40a845bce9ed29ad40598';
	}

	// 接続
	$request_options = array(
		'http' => array(
			'method' => 'GET',
			'header'=> "Authorization: Bearer " . $code
		),
		'ssl'=> array(
			'verify_peer'=>false,
			'verify_peer_name'=>false,
		),
	);
	$context = stream_context_create($request_options);

	// ショップ情報取得
	$url_shop = 'https://api.shop-pro.jp/v1/shop.json';
	$response_shop = file_get_contents($url_shop, false, $context);
	$shop = json_decode($response_shop, true);

?>

<button id="page-top" class="page-top"></button>
<div class="instagram"></div>
<script>
	$.ajax({
	type: 'GET',
	url: 'https://graph.facebook.com/v13.0/17841451334446127?fields=name,media.limit(6){caption,media_url,thumbnail_url,permalink,like_count,comments_count,media_type}&access_token=EAAFWKUBpR8IBAOAJxbSJik8uqZCjmPhX1npADcmDZAij7TJo1fGlXXXO8lnpQiXZAiDgVmBtl0ZAwkizg69rwS35fwOAk1SxH8aPtQxZBjBQ4bOZC6TNoZBOPy1mVZAt3nH0x8nauzigR0STibZC0Hte9raeoAAnoNsMLldx4DhaE4Py39DbhFvuBZCCACniqKBTMZD',
	dataType: 'json',
	success: function(json) {		    	
    var ig = json.media.data;
    for (var i = 0; i < ig.length; i++) {
    	$('.instagram').append('<div><a href="' + ig[i].permalink + '" target="_blank"><img src="' + ig[i].media_url + '></a></div>');
    }
    //$(".instagram").append(html);
	}
});
</script>
<section id="information" class="layout">
	<div class="layout_left">
		<div class="left_content">
			<h2>information</h2>
			<ul class="informatin_ul fadeUpTrigger">
				<li>住所</li>
				<li>〒602-8383<br>
					京都市上京区今出川通御前東入る西今小路町809番地<br>
					<small>（北野天満宮すぐ横／市バス「北野天満宮前」徒歩1分・嵐電 「北野白梅町駅」徒歩5分）</small></li>
				<li>電話</li>
				<li>075-432-7616</li>
				<li>営業日</li>
				<li>金・土・日・祝・25日</li>
				<li>営業時間</li>
				<li>10:00 〜 18:00</li>
			</ul>
		</div>
		<div class="layout_right fadeUpTrigger">
			<iframe src="https://www.google.com/maps/d/embed?mid=1u6JbWS7gy8HGa0GdLMCZYb9syfwmFjQ0&hl=ja&ehbc=2E312F"></iframe>
		</div>
	</div>
</section>

<footer>
	<ul>
		<li>
			<a href="<?php echo home_url(); ?>">
				top
			</a>
		</li>
		<li>
			<a href="<?php echo home_url('/story/'); ?>">
				story
			</a>
		</li>
		<li>
			<a href="<?php echo home_url('/menu/'); ?>">
				menu
			</a>
		</li>
		<li>
			<a href="<?php echo home_url('/store/'); ?>">
				store
			</a>
		</li>
		<li>
			<a href="<?php echo home_url('/category/news/'); ?>">
				news
			</a>
		</li>
		<li>
			<a href="<?php echo home_url('/category/blog/'); ?>">
				blog
			</a>
		</li>
		<li>
			<a href="<?php echo home_url('/contact/'); ?>">
				contact
			</a>
		</li>
		<li>
			<a href="<?php echo $shop['shop']['url']; ?>" target="_blank">onlineshop</a>
		</li>
	</ul>
	<div class="footer_content">
		<div class="footer_content_store">
			<img class="logo" src="<?php echo get_template_directory_uri(); ?>/img/colorer_logo_bl.png" alt="Confitta Colorer ロゴ">
			<div>
				Confitta Colorer［コンフィッタ・クロレ］
				<a href="https://www.instagram.com/confitta_colorer/" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/img/instagram.png" alt="Confitta Colorer Instagram">
				</a>
			</div>
			<a href="https://kitano-lab.com/index.html" target="_blank"><img class="logo logo_2" src="<?php echo get_template_directory_uri(); ?>/img/kitanolab.png" alt="北野ラボ ロゴ"></a>
			<div>
				<span>姉妹店</span>
				コンフィチュール・シロップ専門店
			</div>
			<div>
				<a href="https://kitano-lab.com/index.html" target="_blank">
					北野ラボ
				</a>
				<a href="https://www.instagram.com/kitanolab/" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/img/instagram.png" alt="北野ラボInstagram">
				</a>
			</div>
		</div>
		<div class="footer_content_ltd">
			<p>[運営元]</p>
			<p><a href="https://gosaika.com/" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/img/gosaika_logo.png" alt="株式会社伍彩菓">
			</a></p>
			<p>〒602-8386</p>
			<p>京都府京都市上京区御前通一条上る馬喰町914番地</p>
		</div>
	</div>
	<small style="display:block; margin-bottom:10px; color: #9E9797; padding: 0 20px;">
		このサイトはreCAPTCHAによって保護されており、Googleの
		<a href="https://policies.google.com/privacy">プライバシーポリシー</a>と
		<a href="https://policies.google.com/terms">利用規約</a>が適用されます。
	</small>
	<small>Copyright &copy; <?php echo date('Y'); ?> GOSAIKA Co,Ltd. All Rights Reserved.</small>
</footer>

<?php wp_footer(); ?>

</body>
</html> 