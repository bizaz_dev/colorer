<?php get_header(); ?>
<?php

	if (substr(home_url(), 0, 16) == 'http://localhost') {
		$code = 'ec72d09911962cc71aef2e9e8f4c646a529d93748583706f5961fcca1e239333';
	} elseif (substr(home_url(), 0, 15) == 'http://bizaz.jp') {
		$code = '6be3f89685dba46977f474d4edca6db4ac1573e6ad9e53595168eb70c4f5b463';
	} elseif (substr(home_url(), 0, 15) == 'https://gosaika') {
		$code = 'bebae537ffa558f7b7b869897f42a73b3d8584d4f6bef37b48dac45f92cbf227';
	} elseif (substr(home_url(), 0, 16) == 'https://confitta') {
		$code = '4906df20217b44ab7091d5ed2155b6bd13d5e03e35c40a845bce9ed29ad40598';
	}

	// 接続
	$request_options = array(
		'http' => array(
			'method' => 'GET',
			'header'=> "Authorization: Bearer " . $code
		),
		'ssl'=> array(
			'verify_peer'=>false,
			'verify_peer_name'=>false,
		),
	);
	$context = stream_context_create($request_options);

	// ショップ情報取得
	$url_shop = 'https://api.shop-pro.jp/v1/shop.json';
	$response_shop = file_get_contents($url_shop, false, $context);
	$shop = json_decode($response_shop, true);

?>

<!-- ヘッダー -->
<div class="detail_header">
    <div class="detail_h_img">
        <img src="<?php echo get_template_directory_uri(); ?>/img/header.jpg" alt="<?php the_title(); ?>">
    </div>
</div>

<?php get_template_part('breadcrumb'); ?>

<div class="detail_big_container">
    <?php
		
		if (substr($_SERVER['REQUEST_URI'], -7) == '/story/') {
			$css = 'about_container';
		} elseif (substr($_SERVER['REQUEST_URI'], -7) == '/store/') {
			$css = 'store_container';
        }

    ?>
    <div class="detail_container <?php echo $css; ?>">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <?php the_content(); ?>

        <?php endwhile; else : ?>

        <p><?php _e('記事がありません'); ?></p>
        <?php endif; ?>

    </div>
</div>

<a href="<?php echo $shop['shop']['url']; ?>" class="onlineshop_bunner page_bunner fadeUpTrigger" target="_blank">
	<div><img src="<?php echo get_template_directory_uri(); ?>/img/dummy-2.jpg" alt="オンラインショップ"></div>
	<p>ONLINE SHOP</p>
</a>

<?php get_footer(); ?>