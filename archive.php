<?php get_header(); ?>
<?php

    if (substr(home_url(), 0, 16) == 'http://localhost') {
        $code = 'ec72d09911962cc71aef2e9e8f4c646a529d93748583706f5961fcca1e239333';
    } elseif (substr(home_url(), 0, 15) == 'http://bizaz.jp') {
        $code = '6be3f89685dba46977f474d4edca6db4ac1573e6ad9e53595168eb70c4f5b463';
    } elseif (substr(home_url(), 0, 15) == 'https://gosaika') {
        $code = 'bebae537ffa558f7b7b869897f42a73b3d8584d4f6bef37b48dac45f92cbf227';
    } elseif (substr(home_url(), 0, 16) == 'https://confitta') {
        $code = '4906df20217b44ab7091d5ed2155b6bd13d5e03e35c40a845bce9ed29ad40598';
    }

	// 接続
	$request_options = array(
		'http' => array(
			'method' => 'GET',
			'header'=> "Authorization: Bearer " . $code
		),
		'ssl'=> array(
			'verify_peer'=>false,
			'verify_peer_name'=>false,
		),
	);
	$context = stream_context_create($request_options);

	// ショップ情報取得
	$url_shop = 'https://api.shop-pro.jp/v1/shop.json';
	$response_shop = file_get_contents($url_shop, false, $context);
	$shop = json_decode($response_shop, true);

?>

<!-- ヘッダー -->
<div class="detail_header">
    <div class="detail_h_img">
        <img src="<?php echo get_template_directory_uri(); ?>/img/header.jpg" alt="<?php the_title(); ?>">
    </div>
</div>

<?php get_template_part('breadcrumb'); ?>

<div class="detail_big_container category">
    <?php
        $category = get_the_category();
        $cat_name = $category[0]->cat_name;
    ?>
    <h2><?php echo $cat_name; ?></h2>
    <p class="h2_subtext"><?php echo ($cat_name == 'blog')? '全国農家探訪': 'お知らせ';?></p>
    <div class="detail_container">

        <?php if ($cat_name == 'news') { ?>
        <ul class="tags">
            <a href="<?php echo home_url('/category/news/'); ?>" class="tag tag_all">すべて</a>
        <?php
            $term_list = get_terms('post_tag');
            $result_list = [];
            foreach ($term_list as $term) {
                if ($term->name == '新商品') {
                    $css = 'tag_new';
                } elseif ($term->name == 'その他') {
                    $css = 'tag_other';
                } else {
                    $css = '';
                }
                $u = (get_term_link( $term, 'post_tag' ));
                echo '<a href="'. $u .'" class="tag '. $css .'">'. $term->name .'</a>';
            }
        ?>
        </ul>
        <?php } ?>

        <ul class="news_ul">
        <?php
            parse_str( $query_string, $args );
            $args = array_merge( $args, array(
                'post_type' => 'post',
                'order' => 'DESC',
                'posts_per_page' => 4,
                'paged' => get_query_var( 'paged', 1 ),
                )
            );
            $pickupitem = new WP_Query($args);

            if($pickupitem -> have_posts()) :
                $count = 0;
            while ($pickupitem -> have_posts()) : $pickupitem -> the_post();
        ?>

            <li>
                <time><?php the_time('Y.m.d'); ?></time>
                <a href="<?php echo home_url('/?p=' . $pickupitem->posts[$count]->ID); ?>" class="contents">
                    <div class="news-img">
                        <?php
                        
                        if (the_post_thumbnail('full')) {
                            echo the_post_thumbnail('full'); 
                        } else {
                            echo '<img class="s_logo" src="' . get_template_directory_uri() . '/img/colorer_logo_wh.png" alt="サムネイル">';
                        }
                        
                        ?>
                    </div>
                    <div class="news-caption">
                        <?php
                        $posttags = get_the_tags();
                        if ( $posttags ) {
                            echo '<div class="captions">';
                            foreach ( $posttags as $tag ) {
                                if ($tag->name == '新商品') {
                                    $css = 'tag_new';
                                } elseif ($tag->name == 'その他') {
                                    $css = 'tag_other';
                                } else {
                                    $css = '';
                                }
                                echo '<span class="tag '. $css .'">' . $tag->name .'</span>';
                            }
                            echo '</div>';
                        }
                        ?>
                        <h2><?php echo the_title(); ?></h2>
                    </div>
                </a>
            </li>

        <?php 
            $count++;
            endwhile; else : 
        ?>

        <p><?php _e('記事がありません'); ?></p>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
        </ul>
        <?php
        $args = array(
            'mid_size' => 1,
            'prev_next' => true,
            'prev_text' => __( '前へ'), 
            'next_text' => __( '次へ'),
        );
        the_posts_pagination($args);
        ?>
    </div>
</div>

<section class="instagram">
    <h2>Instagram Gallery</h2>
    <div class="fadeUpTrigger"><?php echo do_shortcode('[instagram-feed]'); ?></div>
</section>

<a href="<?php echo $shop['shop']['url']; ?>" class="onlineshop_bunner fadeUpTrigger" target="_blank" style="margin-top: 80px;">
	<div><img src="<?php echo get_template_directory_uri(); ?>/img/dummy-2.jpg" alt="オンラインショップ"></div>
	<p>ONLINE SHOP</p>
</a>

<?php get_footer(); ?>