<?php
/*
Template Name: menu
*/
?>

<?php get_header(); ?>

<!-- ヘッダー -->
<div class="detail_header">
    <div class="detail_h_img">
        <img src="<?php echo get_template_directory_uri(); ?>/img/header.jpg" alt="<?php the_title(); ?>">
    </div>
</div>

<?php get_template_part('breadcrumb'); ?>

<div class="detail_big_container">
    <div class="detail_container menu_container">

        <?php the_content(); ?>

        <?php
            if (substr(home_url(), 0, 16) == 'http://localhost') {
                $code = 'ec72d09911962cc71aef2e9e8f4c646a529d93748583706f5961fcca1e239333';
            } elseif (substr(home_url(), 0, 15) == 'http://bizaz.jp') {
                $code = '6be3f89685dba46977f474d4edca6db4ac1573e6ad9e53595168eb70c4f5b463';
            } elseif (substr(home_url(), 0, 15) == 'https://gosaika') {
				$code = 'bebae537ffa558f7b7b869897f42a73b3d8584d4f6bef37b48dac45f92cbf227';
			} elseif (substr(home_url(), 0, 16) == 'https://confitta') {
				$code = '4906df20217b44ab7091d5ed2155b6bd13d5e03e35c40a845bce9ed29ad40598';
			}

            // 接続
            $request_options = array(
                'http' => array(
                    'method' => 'GET',
                    'header'=> "Authorization: Bearer " . $code
                ),
                'ssl'=> array(
                    'verify_peer'=>false,
                    'verify_peer_name'=>false,
                ),
            );
            $context = stream_context_create($request_options);
            

            // グループ情報取得
            $url = 'https://api.shop-pro.jp/v1/groups.json';
            $response_body = file_get_contents($url, false, $context);
            $groups = json_decode($response_body, true);

            // ColorerのグループIDを取得
            foreach ($groups['groups'] as $this_) {
                if ($this_['name'] == 'Colorer') {
                    $group_id = $this_['id'];
                }
            }


            // カテゴリー情報取得
            $url = 'https://api.shop-pro.jp/v1/categories.json';
            $response_body = file_get_contents($url, false, $context);
            $categories = json_decode($response_body, true);


            // ショップ情報取得
            $url_shop = 'https://api.shop-pro.jp/v1/shop.json';
            $response_shop = file_get_contents($url_shop, false, $context);
            $shop = json_decode($response_shop, true);
            
            
            // CSS用カウント
            $count = 1;
            $cat_count = 0;
        
            // カテゴリー
            echo '<div class="category_button" id="page-link">';
            foreach ($categories['categories'] as $this_) {
                
                // 北野ラボ商品除外
                if ($this_["name"] == 'コンフィチュール' or $this_["name"] == 'シロップ' or $this_["name"] == 'Gateau Confiturier' or $this_["name"] == 'ギフトセット') {
                    continue;
                }

                // カテゴリー名抽出
                $detail = $this_['expl'];
                $start = mb_strpos($detail,'｛')+1;
                $end = mb_strpos($detail,'｝');
                $category_name = mb_substr($detail, $start, $end-$start);

                // カテゴリーボタン
                echo '<a href="#' . $this_["id_big"] . '">' . $category_name . '</a>';
            }
            echo '</div>';
        ?>
        <?php
            // define("OAUTH2_SITE", 'https://api.shop-pro.jp');
            // define("OAUTH2_CLIENT_ID",'7fb7d5f6d9d1191add136081fa971cfcc0b4a302aad8de80a9fcfc92526b6d1e');
            // define("OAUTH2_CLIENT_SECRET", 'b2f23dc58d91d57a21ef0c9d9733662f931f8e6c3b6a3783ca06ae85c3190168');
            // define("OAUTH2_REDIRECT_URI", home_url('/menu/'));

            // $code = $_GET['code'];

            // //認可ページへリダイレクトする
            // if (empty($code)) {
            //     $params = array(
            //         'client_id'     => OAUTH2_CLIENT_ID,
            //         'redirect_uri'  => OAUTH2_REDIRECT_URI,
            //         'response_type' => 'code',
            //         'scope'         => 'read_products'
            //     );
            //     $auth_url = OAUTH2_SITE . '/oauth/authorize?' . http_build_query($params);
            //     header('Location: ' . $auth_url);
            //     echo '<script>location.replace("' . $auth_url . '"); </script>';
            //     exit;
            // }

            // $params = array(
            //     'client_id' => OAUTH2_CLIENT_ID,
            //     'client_secret' => OAUTH2_CLIENT_SECRET,
            //     'code' => $code,
            //     'grant_type' => 'authorization_code',
            //     'redirect_uri' => OAUTH2_REDIRECT_URI
            // );
            // $request_options = array(
            //     'http' => array(
            //         'method' => 'POST',
            //         'content' => http_build_query($params)
            //     ),
            //     'ssl'=> array(
            //         'verify_peer'=>false,
            //         'verify_peer_name'=>false,
            //     ),
            // );

            // $context = stream_context_create($request_options);

            // $token_url = 'https://api.shop-pro.jp/oauth/token';
            // $response_body = file_get_contents($token_url, false, $context);
            // echo $response_body;

            // カテゴリー
            foreach ($categories['categories'] as $this_) {

                // カテゴリー名抽出
                $detail = $this_['expl'];
                $start = mb_strpos($detail,'｛')+1;
                $end = mb_strpos($detail,'｝');
                $category_name = mb_substr($detail, $start, $end-$start);

                // キャッチコピー抽出
                $detail = $this_['expl'];
                $start = mb_strpos($detail,'【')+1;
                $end = mb_strpos($detail,'】');
                $copy = mb_substr($detail, $start, $end-$start);

                // 説明文
                $expl = mb_substr($detail, $end+1);
                
                // 店舗限定
                if ($this_["display_state"] == "hidden") {
                    $shop_only = '<span class="shop_only">店舗限定</span>';
                } else {
                    $shop_only = false;
                }

                // 北野ラボ商品を除外
                if ($this_["name"] == 'コンフィチュール' or $this_["name"] == 'シロップ' or $this_["name"] == 'Gateau Confiturier' or $this_["name"] == 'ギフトセット') {
                    continue;
                }

                // 奇数の場合
                if ($cat_count % 2 == 0) { ?>
                    <div class="menu_cat_container" id="<?php echo $this_["id_big"]; ?>">
                        <div class="left">
                            <img src="<?php echo $this_['image_url'] . '?cmsp_timestamp=20220105141954'; ?>">
                        </div>
                        <div class="right">
                            <div class="right_content">
                                <?php echo $shop_only; ?>
                                <h5><?php echo $category_name; ?></h5>
                                <h3><?php echo $copy; ?></h3>
                                <p><?php echo $expl; ?></p>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="menu_cat_container reverse" id="<?php echo $this_["id_big"]; ?>">
                        <div class="right">
                            <div class="right_content">
                                <?php echo $shop_only; ?>
                                <h5><?php echo $category_name; ?></h5>
                                <h3><?php echo $copy; ?></h3>
                                <p><?php echo $expl; ?></p>
                            </div>
                        </div>
                        <div class="left">
                            <img src="<?php echo $this_['image_url'] . '?cmsp_timestamp=20220105141954'; ?>">
                        </div>
                    </div>
                
                <?php } ?>

                <?php
                    $query_str = http_build_query( array(
                        'group_ids'   => $group_id, // グループID（Colorerの商品だけ表示）
                        'category_id_big' => $this_['id_big'], // カテゴリーID
                        'display_state'   => 'showing', // 表示中の商品だけを取得 
                    ) );
                    
                    $url = 'https://api.shop-pro.jp/v1/products.json?' . $query_str;
                    $response_body = file_get_contents($url, false, $context);
                    $items = json_decode($response_body, true);

                    // echo $context;
                    // var_dump($items);
                ?>
                <div class="menu_items">
                    <?php
                        if ($items) {
                            // カテゴリー内の偶数奇数判別用
                            $items_count = 1;

                            // 表示順対応
                            if (!empty($items['products'][0]['sort'])) {
                                $items['products'] = sortByKey('sort', SORT_ASC, $items['products']);
                            }

                            // 同カテゴリ内の商品をループで表示
                            foreach ($items['products'] as $items_) { 
                                
                                // 商品名改行
                                $name_r = $items_['name'];
                                $name = str_replace(" -", "<br>-", $name_r);

                                // フォンダンタルト5種セットを除外
                                if ($items_["id"] == 166863702) {
                                    continue;
                                }

                                ?>
                                <a class="items modal-open fadeUpTrigger" data-target="modal-<?php echo $count; ?>">
                                    <div class="menu_img<?php echo $img_css; ?>">
                                        <?php 
                                            if ($items_['image_url']) {
                                                echo '<img src="'. $items_['image_url'] .'">';
                                            } else {
                                                echo '<img src="<?php echo get_template_directory_uri(); ?>/img/dummy-6.png">';
                                            }
                                            
                                        ?>
                                    </div>
                                    <div class="content">
                                        <?php
                                            if ($items_['smartphone_expl']) {
                                                echo '<span>'. $items_['smartphone_expl'] . '</span>';
                                            }
                                        ?>
                                        <h5><?php echo $name; ?></h5>
                                        <p><?php echo $items_['expl']; ?></p>
                                        <span class="next"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-next_wh.svg"></span>
                                    </div>
                                </a>
                                
                                <!-- モーダルウィンドウ -->
                                <div class="modal-wrapper" id="modal-<?php echo $count; ?>">
                                    <a href="!#" class="modal-overlay"></a>
                                    <div class="modal-window">
                                        <div class="modal-content">
                                            <?php 
                                                $items_['smartphone_expl'];
                                                // 2枚目以降ある場合、slickで複数表示
                                                if ($items_['images']) {
                                                    echo '<div class="swiper"><div class="swiper-wrapper">';
                                                    echo '<div class="swiper-slide"><img src="'. $items_['image_url'] .'"></div>';
                                                    foreach ($items_['images'] as $images) {
                                                        echo '<div class="swiper-slide"><img src="'. $images['src'] .'"></div>';
                                                    }
                                                    echo '</div>';
                                                    echo '<div class="swiper-pagination"></div>';
                                                    echo '<div class="swiper-button-prev"></div>';
                                                    echo '<div class="swiper-button-next"></div></div>';
                                                } else {
                                                    echo '<div class="modal_img"><img src="'. $items_['image_url'] .'"></div>';
                                                }
                                                
                                            ?>
                                            <div class="content">
                                                <?php
                                                    if ($items_['smartphone_expl']) {
                                                        echo '<span class="from">'. $items_['smartphone_expl'] . '</span>';
                                                    }
                                                ?>
                                                <h2><?php echo $name; ?></h2>
                                                <!-- <div>¥ <?php echo $items_['sales_price_including_tax']; ?><span>（税込）</span></div> -->
                                                <p><?php echo $items_['expl']; ?></p>
                                                <a href="<?php echo $shop['shop']['url'] . '/?pid=' . $items_['id']; ?>" class="button" target="_blank">この商品を買う</a>
                                            </div>
                                        </div>
                                        <a href="!#" class="modal-close">× 閉じる</a>
                                    </div>
                                </div>

                                <?php $count++; $items_count++;
                            }
                        }
                    ?>
                </div>
                
            <?php $cat_count++; } ?>

    </div>
</div>

<a href="<?php echo $shop['shop']['url']; ?>" class="onlineshop_bunner fadeUpTrigger" target="_blank" style="margin-top: 40px;">
	<div><img src="<?php echo get_template_directory_uri(); ?>/img/dummy-2.jpg" alt="オンラインショップ"></div>
	<p>ONLINE SHOP</p>
</a>

<?php get_footer(); ?>