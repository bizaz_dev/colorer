<?php get_header(); ?>

<!-- ヘッダー -->
<div class="detail_header">
    <div class="detail_h_img">
        <img src="<?php echo get_template_directory_uri(); ?>/img/header.jpg" alt="<?php the_title(); ?>">
    </div>
</div>

<?php get_template_part('breadcrumb'); ?>
<?php
    $category = get_the_category();
    $cat_name = $category[0]->cat_name;

    switch ($cat_name) {
        case 'blog':
            $class = 'blog_detail';
            break;
        
        default:
            $class = '';
            break;
    }
?>

<div class="detail_big_container category <?php echo $class; ?>">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <!-- コンテンツ -->
        <div class="detail_container">

            <!-- タイトル -->
            <h2><?php the_title(); ?></h2>

            <div class="single_container">
                <div class="date"><?php echo get_the_date(); ?></div>
                <?php the_content(); ?>
            
                <div class="prev_next_link">
                    <!-- 前の記事へ -->
                    <?php
                        // if (get_previous_post()) {
                            if (previous_post_link('%link', '<img src="'. get_template_directory_uri().'/img/arrow-prev.svg" alt="前のニュースへ" width="12"/>%title', true)) {
                                echo previous_post_link('%link', '<img src="'. get_template_directory_uri().'/img/arrow-prev.svg" alt="前のニュースへ" width="12"/>%title', true);
                            } else {
                                echo '<a class="hide"></a>';
                            }
                        // }
                    ?>

                    <!-- 次の記事へ -->
                    <?php
                        // if (get_next_post()) {
                            if (next_post_link('%link', '%title<img src="'. get_template_directory_uri().'/img/arrow-next.svg" alt="次のニュースへ" width="12"/>', true)) {
                                echo next_post_link('%link', '%title<img src="'. get_template_directory_uri().'/img/arrow-next.svg" alt="次のニュースへ" width="12"/>', true);
                            } else {
                                // echo '<a class="hide"></a>';
                            }
                        // }
                    ?>
                </div>

                <a href="<?php echo home_url('/category/' . $cat_name . '/'); ?>" class="return_link">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-prev.svg" alt="blogへ" width="12"/>
                    <?php echo $cat_name; ?> 一覧に戻る
                </a>
                
                <?php endwhile; else : ?>
                <p><?php _e('記事がありません'); ?></p>
                <?php endif; ?>
            </div>
        </div>
</div>

<?php get_footer(); ?>